import Vue from 'vue'
import App from './App.vue'
import Leaderboard from './components/LeaderboardPage.vue'

Vue.config.productionTip = false

new Vue({
  data: {
    main: Leaderboard
  },
  render: h => h(App)
}).$mount('#app')
