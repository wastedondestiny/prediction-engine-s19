require('dotenv').config();

const fs = require('fs').promises;
const express = require('express');
const puppeteer = require('puppeteer');

(async () => {
  // Create a simple Express server that prints "Hello, World"
  const app = express();
  app.use(express.static('./dist'));
  const server = app.listen(); // Take a random available port
  
  // Launch Puppeteer and navigate to the Express server
  const browser = await puppeteer.launch({ slowMo: 500 });

  const indexPage = await browser.newPage();
  await indexPage.goto(`http://localhost:${server.address().port}`);
  const indexContent = await indexPage.content();
  await fs.writeFile('./dist/index.html', indexContent);
  
  console.log(process.env.VUE_APP_USE_LEADERBOARD);

  if (process.env.VUE_APP_USE_LEADERBOARD) {
    const leaderboardPage = await browser.newPage();
    await leaderboardPage.goto(`http://localhost:${server.address().port}/leaderboard.html`);
    const leaderboardContent = await leaderboardPage.content();
    await fs.writeFile('./dist/leaderboard.html', leaderboardContent);
  }
  
  // Cleanup 
  await browser.close();
  server.close();
})()
