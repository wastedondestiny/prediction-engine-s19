require('dotenv').config();
const { defineConfig } = require('@vue/cli-service');

const pages = {
  index: {
    entry: 'src/main.js',
    template: 'public/index.html',
    filename: 'index.html'
  },
  redirect: {
    entry: 'src/redirect.js',
    template: 'public/redirect.html',
    filename: 'redirect.html'
  }
}

console.log(process.env.VUE_APP_USE_LEADERBOARD);

if (process.env.VUE_APP_USE_LEADERBOARD) {
  pages.leaderboard = {
    entry: 'src/leaderboard.js',
    template: 'public/index.html',
    filename: 'leaderboard.html'
  };
}

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: '',
  pages
});
